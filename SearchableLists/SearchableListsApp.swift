//
//  SearchableListsApp.swift
//  SearchableLists
//
//  Created by Robert Voogdgeert on 02/02/2024.
//

import SwiftUI

@main
struct SearchableListsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
